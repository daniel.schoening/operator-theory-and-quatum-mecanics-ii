\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{hrflecture}[2020/04/19 For lecture notes]

% actual documentclass
\LoadClass[parskip=half-,a4paper, headsepline, chapterprefix=true]{scrreprt}    

% class options
\newif\iffilemanagement\filemanagementfalse
\DeclareOption{filemanagement}{\filemanagementtrue}
\DeclareOption*{\PassOptionsToPackage{\CurrentOption}{hrftex}}
\ProcessOptions\relax

% needed for hrftex if to be numbered within lecture
\newcounter{lecture}
\newcounter{file}
\newcounter{scriptpage}
% main package
\RequirePackage{hrftex}
\RequirePackage{hrfmath}

% escape underscores, useful for files
\DeclareRobustCommand*{\escapeus}[1]{%
  \begingroup\@activeus\scantokens{#1\endinput}\endgroup}
\begingroup\lccode`\~=`\_\relax
   \lowercase{\endgroup\def\@activeus{\catcode`\_=\active \let~\_}}


% for \lecture macro, writes to margin
\DeclareNewTOC[owner=hrflecture,listname=List of lectures,type=lecture,types=lectures, name=Lecture]{listoflectures}
\def\@lecture{}
\newcommand{\lecture}[2][]{%
    \stepcounter{lecture} % be sure to reset all dependent counters
    \@ifnextchar\bgroup{\lecture@double{#1}{#2}}{\lecture@single{#1}{#2}} % title possilble in second (third) argument
    \subsection*{\@lecture}%
    \marginpar{\hfuzz=10pt\small\textsf{\mbox{#2}}}%
}
\newcommand{\lecture@single}[2]{
    \ifstrempty{#1}{\addxcontentsline{listoflectures}{section}[\thelecture]{#2}}{\addxcontentsline{listoflectures}{section}[#1]{#2}}%
    \def\@lecture{Lecture \arabic{lecture}}%
}
\newcommand{\lecture@double}[3]{
    \ifstrempty{#1}{\addxcontentsline{listoflectures}{section}{#3}}{\addxcontentsline{listoflectures}{section}[#1]{#3}}%
    \def\@lecture{Lecture \arabic{lecture}: #3}%
}

\iffilemanagement
    \DeclareNewTOC[owner=hrflecture,listname=List of files,type=file,types=files, name=File]{listoffiles}
    \def\@file{}
    \newcommand{\file}[2][]{
        \stepcounter{file} % be sure to reset all dependent counters
        \ifstrempty{#1}{\addxcontentsline{listoffiles}{section}[\thefile]{\escapeus{#2}}}{\addxcontentsline{listoffiles}{section}[#1]{\escapeus{#2}}}%
        \def\@file{\escapeus{#2}}%
        \marginpar{\raggedright\tiny\@file}%
    }
\else\fi


%writing the page from the original script to margin
\iffilemanagement
    \newcommand*{\page}{
        \@ifnextchar\bgroup{\page@single}{\page@none}%
    }
    \newcommand{\page@none}{
        \stepcounter{scriptpage}
        \marginpar{\raggedright\tiny p. \thescriptpage}
    }
    \newcommand{\page@single}[1]{
        \setcounter{scriptpage}{#1}
        \marginpar{\raggedright\tiny p. \thescriptpage}
    }
\else\fi


% header, chapter inner, lecture outer 
\automark{chapter}
\renewcommand{\chaptermarkformat}{%
    \chapappifchapterprefix{\ }\thechapter:\enskip}
\chead{}
\iffilemanagement
    \KOMAoptions{headlines=2.1}
    \ohead{\@lecture\\\@file}
    \ihead{\leftmark\\}
    \recalctypearea
\else
    \ihead{\leftmark}
    \ohead{\@lecture}
\fi



% Document settings
\AtBeginDocument{\pagenumbering{arabic}}
\setlength{\parindent}{0em}

% We don't need _that_ many warnings
\hfuzz=10pt

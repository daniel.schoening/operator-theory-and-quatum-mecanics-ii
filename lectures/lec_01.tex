\chapter{Basics on von Neumann algebras}
\lecture{19.04.22 10:15}
\file{OpTh2_01_v2.pdf}
\page{1}

\begin{notations/conventions*}\

    \begin{enumerate}
        \item[$\mathcal{H}$:] (separable) complex Hilbert space with scalar product linear in the \emph{second} argument.
        \item[$\mathcal{B}(\mathcal{H})$:] bounded operators on $\mathcal{H}$, i.e.
        \begin{equation*}
            A: \mathcal{H}\to \mathcal{H}, \opnorm{A} = \sup_{u\in \mathcal{H}\\ u\neq 0} \frac{\norm{Au}}{\norm{u}}<\infty
        \end{equation*}
    \end{enumerate}
\end{notations/conventions*}

\begin{definition}
    $\mathscr{X} \subset \mathcal{B}(\mathcal{H}), \mathscr{V}\subset \mathcal{H}$ subsets,
    $\mathscr{X}\mathscr{V} \coloneqq \set{Av}{A\in\mathscr{X}, v\in\mathscr{V}}$,\\
    $[\mathscr{X}\mathscr{V}]\coloneqq$ closure of the linear span of $\mathscr{X}\mathscr{V}$ (the closed subspace \emph{generated} by $\mathscr{X}\mathscr{V}$).\\
    Notation: For $\mathscr{V} = \set{v}, v\in \mathcal{H}$, we write $\mathscr{X}v$ and $[\mathscr{X}v]$, respectively.\\
    Recall:$\exists!$ projection onto $[\mathscr{X}\mathscr{V}]$ denoted $P_{[\mathscr{X}\mathscr{V}]}$ or just $[\mathscr{X}\mathscr{V}]$.
\end{definition}

\begin{definition}\index{commutant}\index{bi-commutant}
    For a subset $\mathscr{X} \subset \mathcal{B}(\mathcal{H})$,
    call $\mathscr{X}^\prime\coloneqq\set{B\in \mathcal{B}(\mathcal{H})}{BA = AB \forall A \in \mathcal{B}(\mathcal{H})}$
    the \underline{commutant} of $\mathscr{X}$, and 
    \page
    $\left(\mathscr{X}^\prime\right)^\prime =$ the \underline{bi-commutant} (or double commutant) of $\mathscr{X}$.
    $\mathscr{X}^{(n)}$ denotes the $n$-th commutant, $\mathscr{X}^{(n)} = \left(\left(\mathscr{X}^\prime\right)^\prime\ldots\right)^\prime$.
\end{definition}

\begin{remark}\
    
    \begin{enumerate}[label=\roman*)]
        \item $X\subset X^\dprime$ ($A\in X, B\in X^\prime$ then$ AB=BA$)\\
        This inclusion my strict $\rightarrow$ examples later,
        but we have $X^{(2k-1)} = X^\prime$  and $X^{(2k)} = X^\dprime$ for any $k\geq 1$,
        but in general $X\neq X^\dprime$
        \newpage
        \item $\mathscr{X}^\prime$ is a unital (containing $I$) algebra, closed w.r.t. the strong operator topology
        (defined by the family $\mathcal{B}(\mathcal{H})\ni A\mapsto\norm{Au}\forall u\in \mathcal{H}$)
        \begin{proof}
            Unital: \checkmark\\
            \settowidth{\impwit}{Closed: $\set{B_\alpha} \to B$ in the strong op. top. }
            Closed: $\set{B_\alpha} \to B$ in the strong op. top. 
            $\iff \norm{B_\alpha u} \to \norm{B u}\forall u\in \mathcal{H}$\\\hspace*{\impwit}
            $\iff \stronglim B_\alpha u = Bu \forall u\in \mathcal{H}$\\
            So if $\set{B_\alpha}\subset\mathscr{X}^\prime$,
            then $\forall A\in \mathscr{X}:\, ABu = \stronglim AB_\alpha u = \stronglim B_\alpha A u = BAu$.
        \end{proof}
        \page 
        \item $\mathscr{X}^\prime$ is also closed in the norm topology
        ($\opnorm{AB-BA} \leq \opnorm{AB-AB_\alpha} + \opnorm{AB_\alpha + BA}$ and $\opnorm{AB-AB_\alpha} \leq \opnorm{A}\opnorm{B-B_\alpha}$)
        This has a deeper explanation: The strong topology is \emph{weaker} than the norm topology.
        The commutant of any subset $\mathscr{X}\subset \mathcal{B}(\mathcal{H})$ is a \hyperref[th:1.{5}]{von Neumann algebra}.
    \end{enumerate}
\end{remark}

\begin{definition}\index{Banach algebra}\index{C*-algebra}
    A \underline{Banach algebra} $\mathscr{A}$ (i.e. an algebra on $\C$ with a topology given by a norm $\norm{\cdot}$,
    which is complete w.r.t. this norm and satisfies $\norm{A\cdot B}\leq\norm{A}\cdot\norm{B}$ ``submultiplicativity'')
    which moreover has an \underline{involution} $*:\mathscr{A}\to\mathscr{A}$
    (i.e. the *-algebra properties $\left(A^*\right)^* = A, (A\cdot B)^* = B^*\cdot A^*,(zA +wB)^* =  \ol{z}A^* + \ol{w}B^*$ and
    the additional property of *-algebras wit norm $\norm{A^*} = \norm{A}$ hold) and satisfies the C*-property $\norm{A^*A} = \norm{A}^2$ is called a \underline{C*-algebra}
\end{definition}

\begin{note*}
    Any *-algebra of operators $\mathscr{A}\subset \mathcal{B}(\mathcal{H})$, which is closed in the operator norm is a C*-algebra. 
\end{note*}

\page

A special class of C*-algebras:

\begin{definition}\index{von Neumann algebra}\label{th:\thegeneralthm}
    A *-algebra of operators, $\mathscr{M}\subset \mathcal{B}(\mathcal{H})$ which contains $I\in \mathcal{B}(\mathcal{H})$,
    and is closed   in the strong topology, is called a \underline{von Neumann algebra}.
\end{definition}

\begin{remark}
    Since the norm top. is stronger than the strong op.top. and therefore coarser, any von Neumann algebra is a C*-algebra.
    The converse is not true in, for example the space of compact operators $\mathcal{K}(\mathcal{H})$ is not closed in the strong op. top.
\end{remark}

\begin{theorem}[von Neumann denseness theorem]\index{von Neumann denseness theorem}\label{th:\thegeneralthm}\label{th:v.N.}

    \begin{enumerate}
        \item[] A unital *-algebra of operators, $\mathscr{M}\subset \mathcal{B}(\mathcal{H})$ is a von Neumann algebra
        (i.e. closed in the strong op. topology)\label{th:\thegeneralthm-1}\marginpar{(1)}
        \item[$\iff$] $\mathscr{M} = \mathscr{M}^\dprime$\label{th:\thegeneralthm-2}\marginpar{(2)}
        \item[$\iff$] $\mathscr{M}$ is closed w.r.t. the weak op. topology.\label{th:\thegeneralthm-3}\marginpar{(3)}
    \end{enumerate}
\end{theorem}

\begin{proof}\

    \begin{enumerate}
        \settowidth{\impwit}{(1) $\implies$ }
        \itemindent\impwit
        \item[{\hyperref[th:1.{7}-3]{(3)} $\implies$ \hyperref[th:1.{7}-1]{(1)}}]
        \checkmark (the weak op. topology is weaker than the strong op. topology)
        \item[{\hyperref[th:1.{7}-1]{(1)} $\implies$ \hyperref[th:1.{7}-2]{(2)}}]
        For any $\mathscr{X}\subset \mathcal{B}(\mathcal{H})$ we have
        $\mathscr{X}^\prime = \bigcap_{A\in\mathscr{X}}\set{B\in \mathcal{B}(\mathcal{H})}{AB-BA=0}$, which is closed in the weak op. top.
        (as $A\mapsto AB-BA$ is continuous w.r.t. the weak op.top.)
        \page
        \item[{\hyperref[th:1.{7}-2]{(2)} $\implies$ \hyperref[th:1.{7}-3]{(3)}}]
        Reformulate in terms of
    \end{enumerate}
    \begin{lemma}\label{th:\thegeneralthm}
        Let $\mathscr{A}\subset \mathcal{B}(\mathcal{H})$ be a unital *-subalgebra.
        Then $\mathscr{A}$ is dense in $\mathscr{A}^\dprime$ w.r.t the strong op. top.
    \end{lemma}
    
    
    \begin{proof}[Proof of lemma \ref{th:1.{8}}]
        First consider a subbasis for the topology: given $A_0\in\mathscr{A}^\dprime\subset \mathcal{B}(\mathcal{H})$, consider
        \begin{equation*}
            U = \set{A\in \mathcal{B}(\mathcal{H})}{\norm{A_0v - Av}<\varepsilon}
        \end{equation*}
        for some $v\in \mathcal{H}$. We show that $U$ contains an element of $\mathscr{A}$.
        Equivalently, we show that $A_0v\in[\mathscr{A}v] = \ol{\mathscr{A}v}\subset \mathcal{H}$.\newline
        By definition, $[\mathscr{A}v]$ is $\mathscr{A}$-invariant, $\mathscr{A}^* = \mathscr{A}$,
        and hence $[\mathscr{A}v]\orth$ is also $\mathscr{A}$-inavriant, which in turn implies $P\in \mathscr{A}^\prime$. \newline
        Therefore, $P$ commutes with $A_0\in \mathscr{A}^\dprime$ and so, $PA_0v = A_0Pv = A_0v$ ($v\in [\mathscr{A}v]$ since $I\in \mathscr{A}$)
        which implies $A_0\in [\mathscr{A}v]$. \newline
        This is not yet sufficient to prove denseness because we only considered a subbasis, we have to prove that for any finite sequence $v_1, \ldots, v_n$ and any $\varepsilon>0$,
        we can find an element $A\in \mathscr{A}$
        \page
        such that $\norm{Av_j-A_0v_j}\to 0\forall 1\leq j\leq n$. \newline
        \emph{Trick}: Consider $\mathcal{H}^{\oplus n}$ and let $\mathscr{A}$ act on $\mathcal{H}^{\oplus n}$ by $A(u_1,\ldots,u_n) = (Au_1,\ldots,Au_n) \forall a\in \mathscr{A}$.
        This is the diagonal embedding of $\mathscr{A}$ in $\mathcal{B}\left(\mathcal{H}^{\oplus n}\right) \cong Mat_n(\mathcal{B}(\mathcal{H}))$, call it $\mathscr{A}(n)$.
        Then $\mathscr{A}(n)^\prime \cong Mat_n(\mathscr{A}^\prime)$ and $\mathscr{A}(n)^\dprime$ can be identified with diagonal embedding of
        $\mathscr{A}^\dprime$ in $\mathcal{B}(\mathcal{H}^{\oplus n})$, so $\mathscr{A}(n)^\dprime = \mathscr{A}^\dprime(n)$.
        Therefore, for any $(v_1,\ldots,v_n)\in \mathcal{B}(\mathcal{H}^{\oplus n})$,
        we know that $(A_0v_1,\ldots,A_0v_n)\in[\mathscr{A}(V_1,\ldots,v_n)]\subset \mathcal{H}^{\oplus n}$
        so 
        \begin{equation*}
            \forall \varepsilon>0 \exists A\in \mathscr{A}\st\sum\limits_{1\leq j\leq n} \norm{Av_j - A-0v_j}^2 <\varepsilon^2
        \end{equation*}
        and in particular, $\norm{Av_j-A_=v_j}<\varepsilon \forall j$.
    \end{proof}
    This implies \hyperref[th:1.{7}-2]{(2)} $\implies$ \hyperref[th:1.{7}-3]{(3)} since, if $\mathscr{A}$ is closed w.r.t. the strong op. topology  ,
    it follows that $\mathscr{A} = \ol{\mathscr{A}} \overset{\ref{th:1.{8}}}{=} \mathscr{A}^\dprime$.
\end{proof}

\page

In the proof, it was not important that we considered only \emph{finite} sequences of vectors. In fact, in \ref{th:1.{8}} we really proved

\begin{lemma}
    Let $\mathscr{A}\subset \mathcal{B}(\mathcal{H})$ be a unital *-algebra.
    Let $A_0\in \mathscr{A}^\dprime, \varepsilon>0$, and $v_1,v_2,\ldots\in \mathcal{H}$ with $\sum\norm{v_j}^2 <\infty$ (i.e. $\in \ell^2(\mathcal{H})$).
    Then $\exists A\in \mathscr{A}\st \sum\norm{Av_j - A_ov_j}^2 < \varepsilon$.
\end{lemma}

\newpage 

This motivates 

\begin{definition}\index{ultrastrong topology}
    The \underline{ultrastrong topology} on $\mathcal{B}(\mathcal{H})$ has a basis of sets of the form
    \begin{equation*}
        \set{A\in \mathcal{B}(\mathcal{H})}{\sum\norm{Av_j-A_0v_j}^2<\varepsilon}
    \end{equation*}
    where $\varepsilon>0$ and $v_1,v_2,\ldots\in \mathcal{H}$ s.t. $\sum\norm{v_j}^2 <\infty$
\end{definition}

and we have proven

\begin{lemma}
    A von Neumann algebra is closed w.r.t. the ultrastrong topology.
\end{lemma}

The proof of \ref{th:1.{8}} showed also:

\begin{corollary*}
    $\mathscr{A}$ unital *-algebra $\subset \mathcal{B}(\mathcal{H})$ is dense in $\mathscr{A}^\dprime$ w.r.t the ultarstrong topology,
    so if $\mathscr{A}$ is von Neumann algebra (hence $\mathscr{A} = \mathscr{A}^\dprime$),
    we have $\ol{\mathscr{A}}^{ustr.} = \mathscr{A}^\dprime = \mathscr{A}$.
\end{corollary*}

\page

\begin{definition*}\index{subbasis of topology}
    A \underline{subbasis} of a topology is a collection of open sets, such that finite intersections of its members form a basis of the topology.
\end{definition*}

\begin{remark*}
    Any basis of a top. is a subbasis, but not the reverse, \newline
    e.g. $\set{(-\infty, b)}{b\in\R}\cup\set{a,\infty}{a\in\R}$ is a subbasis of $(R,\abs{\cdot})$ but not a basis
    (for $(c,d)$ bounded, $x\in(c,d)$ there is no $b$ or $a$ s.t. $x\in(-\infty, b)\subset(c,d)$ or $x\in(a,\infty)\subset(c,d)$).
\end{remark*}

For the strong op. top. (which is the weakest top. such that all $\set{B\in \mathcal{B}(\mathcal{H})}{\norm{Bv-A_0v}<1}$ are open,
for any $A_0\in \mathcal{B}(\mathcal{H})$ and any $v\in \mathcal{H}$), a subbasis is given by the family
\begin{equation*}
    U_{A_0,v,\varepsilon} = \set{A\in \mathcal{B}(\mathcal{H})}{\norm{Av - A_0v}<\varepsilon},
\end{equation*}
where $A_0\in \mathcal{B}(\mathcal{H}), v\in \mathcal{H}, \varepsilon>0$.\newline
This is not a basis (consider $U_{A_0,v,\varepsilon}\cap U_{B_0, u, \varepsilon}$ with $u\perp v$),
but the family 
\begin{equation*}
    U_{A_0, v_1, \ldots, v_n, \varepsilon} = \set{A\in \mathcal{B}(\mathcal{H})}{\norm{Av_j - A_0v_j}<\varepsilon, 1\leq j\leq n},
\end{equation*}
where $A_0\in \mathcal{B}(\mathcal{H}), v_j\in \mathcal{H}, \varepsilon>0, n\in\N$, is.

\newpage
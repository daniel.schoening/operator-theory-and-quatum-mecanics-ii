\lecture{26.04.22 12:15}

\begin{notation*}\index{infimum of projections}\index{supremum of projections}
    Let $\set{p_j}_j$ be a family of projections $\subset \mathcal{B}(\mathcal{H})$. Then 
    \begin{align*}
        \bigvee_j p_j &\coloneqq \text{ projection onto } \left[\bigcup_j p_j \mathcal{H}\right]
        \left( = \overline{\sum_jp_j \mathcal{H}}\right)&&\text{ is the ``\underline{supremum}'' of }\set{p_j}_j\\
        \bigwedge_j p_j &\coloneqq \text{ projection onto }\bigcap_jp_j \mathcal{H}&&\text{ is the ``\underline{infimum}'' of }\set{p_j}_j
    \end{align*}
\end{notation*}

\begin{lemma}
    Let $\set{p_j}\subset \mathscr{M}$ be a family of projections in a von Neumann algebra $\mathscr{M}$.
    Then $\bigvee_j p_j\in \mathscr{M}$ and $\bigwedge_j p_j \in \mathscr{M}$.
    Moreover, the set of all central projections such that $pA = A$ for a given fixed $A\in \mathscr{M}$ has a smallest element $z_A$ 
    and 
    \begin{equation*}
        z_A = P_{[(\mathscr{M}A)\mathcal{H}]}.
    \end{equation*} 
\end{lemma}

\begin{proof}\ \newline
    \textbf{1. statement}: $p = \bigvee_j p_j \in \mathcal{B}(\mathcal{H})$. For any unitary $U^\prime\in \mathscr{M}^\prime$ we have
    \begin{equation*}
        U^{\prime*}pU^\prime = U^{\prime*}\Big(\bigvee_j p_j\Big)U^\prime = \bigvee_j \Big(U^{\prime*}p_jU^\prime\Big) = \bigvee_j p_j = p
    \end{equation*}
    and the claim follows from \ref{th:1.{12}}. The proof is analogous for $p = \bigwedge_j p_j$.

    \textbf{2. statement}: $p \mathcal{H} \supset A \mathcal{H} \forall p$ proj. with $pA = A$
    $\implies z_A \mathcal{H} \supset A \mathcal{H} \implies z_A \mathcal{H} = A \mathcal{H}$ (equality since $z_A$ is central).
    By construction, this is the minimal central projection with $z_A A= A$.

    $\mathbf{z_A = P_{[(\mathscr{M}A)\mathcal{H})]}}$: $p = P_{[(\mathscr{M}A)\mathcal{H})]}\in \mathscr{M} \cap \mathscr{M}^\prime$
    (since $[(\mathscr{M}A)\mathcal{H}])$ is invariant under both $\mathscr{M}$ and $\mathscr{M}^\prime$ {\color{red}(I'm not satisfied with this)})
    and as  $[(\mathscr{M}A)\mathcal{H}]\subset A \mathcal{H}$ is follows that
    $pA=A$ $\implies p \geq z_A$. (as $z_A$ is the smallest such projection).\newline
    On the other hand $z_A$ is invariant under $\mathscr{M}$ and $z_A \mathcal{H}\supset A \mathcal{H}$, so $z_A \mathcal{H} \supset [(\mathscr{M}A)\mathcal{H}]$
    and thus $p\leq z_A$.
\end{proof}

\begin{notation/remark*}\index{central support}
    $z_A$ is called the \underline{central support of $A$}.
    Observe that for a projection $P\in \mathscr{M}$, we have $z_p = \bigvee\limits_{\substack{U\in \mathscr{M}\\\text{unitary}}} U^*pU$ (check using $z_p = P_{(\mathscr{M}p)\mathcal{H}}$).
\end{notation/remark*}

\begin{theorem}[Kaplanski's density theorem]\index{Kaplanski's density theorem}\label{th:Kaplanski}\label{th:\thegeneralthm}
    $\mathscr{M}\subset \mathcal{B}(\mathcal{H})$ v. N. alg. $\mathscr{A}\subset \mathscr{M}$ dense (w.r.t. strong op. top.)
    *-subalgebra (i.e. unital). Then the unit ball $\mathscr{A}_1 = \set{A\in \mathscr{A}}{\opnorm{A}\leq 1}$
    (respectively the selfadjoint part of the unit ball $\mathscr{A}^{s.a.}$, the positive part of the unit ball $\mathscr{A}^+$) of $\mathscr{A}$
    is dense in the unit ball$\mathscr{M}_1$ (respectively in $\mathscr{M}^{s.a.}$, in $\mathscr{M}^+$) of $\mathscr{M}$.
\end{theorem}

\begin{remark*}
    This may look like a technical lemma, but, but it will turn out to be very, very useful.
    Its significance is, that if $\mathscr{A}$ is dense in $\mathscr{M}$ w.r.t. the strong op. top.,
    any $B\in \mathscr{M}$ can be approximated w.r.t. the strong op. top. by a net in $\mathscr{A}$,
    by Kaplanski's density theorem, this approximation can be uniformly bounded by $\opnorm{B}$.
\end{remark*}

\begin{proof}
    $\mathscr{X}\subset \mathcal{B}(\mathcal{H})$ subset. Denote by $\mathscr{X}_1, \mathscr{X}^{s.a.}, \mathscr{X}^+$
    the unit ball in $\mathscr{X}$/the self adjoint part of $\mathscr{X}$/the positive part of $\mathscr{X}$.\newline
    w.o.l.o.g. $\mathscr{A}$ is assumed to be closed under the operator norm.
    \begin{enumerate}[label=\protect\circled{\arabic*}]
        \item\label{th:\thegeneralthm-1} $\mathscr{A}^{s.a.} \subset \mathscr{M}^{s.a.}$ is dense w.r.t. strong op. topology.
        \begin{subproof}
            $\mathscr{M}\ni A \mapsto \frac12 (A+A^*)\in \mathscr{M}^{s.a.}$ is continuos w.r.t. weak op. top.
            and restricted to $\mathscr{A}$, its image is $\mathscr{A}^{s.a.}$
            $\implies \mathscr{A}^{s.a.}\subset \mathscr{M}^{s.a.}$ dense w.r.t weak op. topology.
            By the \hyperref[th:v.N.]{von Neumann denseness theorem} it is therefore also dense w.r.t. the strong op. topology.
        \end{subproof}
        \item\label{th:\thegeneralthm-2} $\mathscr{A}_1^{s.a.} \subset \mathscr{M}_1^{s.a.}$ is dense w.r.t. strong op. topology.
        \begin{subproof}
            Let $A\in \mathscr{M}_1^{s.a.}$. There is $B\in \mathscr{M}^{s.a.}$ s.t. $A = 2B\left(1+B^2\right)^{-1}$
            (since $[-1,1]\ni t \mapsto 2t\left(1+t^2\right)^{-1}\in[-1,1]$ is continuos,
            strictly increasing, so it has a continuos inverse, the claim follows by spectral calculus.)\\
            \ref{th:1.{18}-1} and the \hyperref[th:v.N.]{von Neumann denseness theorem} apply:\\
            $\exists\set{B_j}\subset \mathscr{A}^{s.a.}$ converging to $B$ in the strong op. topology.\\
            Define $A_j\coloneqq 2B_j(I + B_j^2)^{-1}$, then $\set{A_j} \subset A_1^{s.a.}$ and 
            \begin{align*}
                A_j-A
                &= (I+B_j^2)^{-1} \left(2B_j(I+B^2) - (I+B_j^2)2B\right) (I+B^2)^{-1}\\
                &= 2(I+B_j^2)^{-1} \underbrace{(B_j-B)}_{\to 0} (I+B^2)^{-1} + 2(I+B_j^2)^{-1}B_j\underbrace{(B-B_j)}_{\to 0}B(I+B^2)^{-1}
            \end{align*}
            $\implies A_j\to A$ (w.r.t. the strong op. top.)\\
            $\implies \mathscr{A}_1^{s.a.}\subset \mathscr{M}_1^{s.a.}$ is dense w.r.t strong op. topology.
        \end{subproof}
        \item $\mathscr{A}_1^+ \subset \mathscr{M}_1^+$ is dense w.r.t. the strong op. topology.
        \begin{subproof}
            Let $A\in \mathscr{M}_1^+, B\coloneqq\sqrt{A}$. By \ref{th:1.{18}-2}
            $\exists \set{B_j}\subset \mathscr{A}_1^{s.a.}, B_j\to B$ (w.r.t. strong op. top.),
            set $A_j = B_j^*B_j\in \mathscr{A}_1^+$. Then $A_j\to A$ (w.r.t. weak op. top.) since
            \begin{align*}
                \abs{\inner{(A-A_j)u}{v}}
                &= \abs{\inner{(B^*B-B_j^*B_j)u}{v}}\\
                &\leq \abs{\inner{B^*(B-B_j)u}{v}} + \abs{\inner{(B^*-B_j^*)B_ju}{v}}\\
                &\leq \abs{\inner{(B-B_j)u}{Bv}} + \abs{\inner{B_ju}{(B-B_j)v}}\\
                &\overset{C.S.}{\leq} \underbrace{\norm{(B-B_j)u}}_{\to 0} \underbrace{\norm{Bv}}_{\leq\norm{v}}
                + \underbrace{\norm{B_ju}}_{\leq\norm{u}} \underbrace{\norm{(B-B_j)v}}_{\to 0}
            \end{align*}
            $\implies \mathscr{A}_1^+ \subset \mathscr{M}_1^+$ is dense w.r.t. the weak op. topology.\\
            $\implies$by \hyperref[th:v.N.]{von Neumann denseness},$\mathscr{A}_1^+ \subset \mathscr{M}_1^+$ is dense w.r.t. the strong op. topology.\\
        \end{subproof}
        \item $\mathscr{A}_1\subset \mathscr{A}_1$ is dense w.r.t. the strong op. topology.
        \begin{subproof}
            Consider $\mathcal{H}\otimes \mathcal{H}$ and $\mat_{2\times 2}(\mathscr{A})\subset \mathcal{B}(\mathcal{H}\times \mathcal{H})$
            and $\mat_{2\times 2}(\mathscr{M})\subset \mathcal{B}(\mathcal{H}\times \mathcal{H})$.
            Clearly, $\mat_{2\times 2}(\mathscr{A})\subset\mat_{2\times 2}(\mathscr{M})$ dense w.r.t. strong op. topology.
            Let $A\in \mathscr{M}_1$. Then $\mqty(0&A^*\\A&0)\in \mat_{2\times 2}(\mathscr{M})$ is selfadjoint and $\opnorm{\cdot}\leq 1$.\\
            \ref{th:1.{18}-2}$\implies\oldexists\set{\mqty(A_{11}^j&A_{12}^j\\A_{21}^j&A_{22}^j)}\subset\left(\mat_{2\times 2}(\mathscr{A})\right)_1^{s.a.}$
            converging to $\mqty(0&A^*\\A&0)$ w.r.t. the strong op. topology. 
            Hence: $\opnorm{A_{21}^j}\leq 1$ and $A_{21}^j\to A$ w.r.t. the strong op. top. and therefore 
            $\mathscr{A}_1\subset \mathscr{A}_1$ is dense in the strong op. topology.
        \end{subproof}
    \end{enumerate}
\end{proof}

\textbf{Added:}
\begin{itemize}
    \item Meaning: Let $\mathscr{A}$ be a (strong op. top.) dense *-subsalgebra$\subset \mathscr{M}\subset \mathcal{B}(\mathcal{H})$
    (example: $\mathscr{M} = \overline{\mathscr{A})}^{st.op.top.}$).
    Then $B\in \mathscr{M}$ can be approximated by a net in $\mathscr{A}$ (by denseness) and by \hyperref[th:Kaplanski]{Kaplanski's theorem}
    even by a net in $\mathscr{A}$ that is \emph{uniformly} bounded by $\opnorm{B}$.
    \item \ref{th:Kaplanski} is the theorem as in Kaplanski's original work.
\end{itemize}
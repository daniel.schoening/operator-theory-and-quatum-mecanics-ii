\lecture{22.04.22 12:15}
\file{OpTh2_02_v2.pdf}
\page

\begin{defremark*}\index{ultraweak topology}\
    \begin{enumerate}[label=\roman*)]
        \item The \underline{ultraweak topology} on $\mathcal{B}(\mathcal{H})$ is given by a basis of the form
        \begin{equation*}
            \set{A\in \mathcal{B}(\mathcal{H})}{\abs{\sum\inner{Av_j-A_0v_j}{w_j}}<\varepsilon}
        \end{equation*}
        where $\varepsilon>0$ and $v_1,v_2,\ldots\in \mathcal{H}, w_1,w_2,\ldots\in \mathcal{H}$ s.t. $\sum\norm{v_j}<\infty, \sum\norm{w_j}<\infty$.
        \item The weak op. topology is weaker than the ultraweak topology (despite the name).
        Hence, a von Neumann algebra is ultraweakly closed.
        {\color{red} If you have a beautiful proof, please send it to me after the corresponding homework deadline}
        \item In fact we have (``$\leq$'' indicating the left-hand side is weaker than the right-hand side) on $\mathcal{B}(\mathcal{H})$.
        
        \begin{tikzpicture}
            \newcommand{\height}{2}
            \node at (-5, 2) (weak) {weak op. top.};
            \node at (0, 2) (strong) {strong op. top.};
            \node at (-5, 0) (ultra weak) {ultra weak top.};
            \node at (0, 0) (ultra strong) {ultra strong top.};
            \node at (5, 0) (norm) {norm    top.};
            \path (norm) -- (ultra strong) node[midway] {$\leq$};
            \path (ultra strong) -- (ultra weak) node[midway] {$\leq$};
            \path (ultra strong) -- (strong) node[midway] {\rotatebox{270}{$\leq$}};
            \path (ultra weak) -- (weak) node[midway] {\rotatebox{270}{$\leq$}};
            \path (strong) -- (weak) node[midway] {$\leq$};
          \end{tikzpicture}
    \end{enumerate}
\end{defremark*}

\page

\begin{center}
    \textbf{A direct characterization of elements of v. N. algebras:}
\end{center}



\begin{lemma}\label{th:\thegeneralthm}
    $\mathscr{M}\subset \mathcal{B}(\mathcal{H})$ von Neumann. $A\in \mathcal{B}(\mathcal{H})$. Then the following are equivalent:
    \begin{enumerate}[label=\alph*)]
        \item\label{th:\thegeneralthm-a} $A\in \mathscr{M}$
        \item\label{th:\thegeneralthm-b} $Ap^\prime = p^\prime A$ for any projection $p^\prime\in \mathscr{M}^\prime$
        \item\label{th:\thegeneralthm-c} ${U^\prime}^* A U^\prime = A$ for any unitary $U^\prime\in \mathscr{M}^\prime$.
    \end{enumerate}
\end{lemma}

\begin{proof}\ \newline
    \ref{th:1.{12}-a} $\implies$ \ref{th:1.{12}-b} and \ref{th:1.{12}-a} $\implies$ \ref{th:1.{12}-c} are true by definition.\newline
    \ref{th:1.{12}-b} $\implies$ \ref{th:1.{12}-a} and \ref{th:1.{12}-c} $\implies$ \ref{th:1.{12}-a}
    mean that $A\in \mathscr{M}^\dprime(\overset{\ref{th:v.N.}}{=} \mathscr{M})$:
    \begin{enumerate}
        \itemindent=32pt
        \item[\ref{th:1.{12}-b} $\implies$ \ref{th:1.{12}-a}:]
        Any selfadjoint $B\in \mathcal{B}(\mathcal{H})$ can be approximated in the norm topology by linear combinations of projections
        by the spectral theorem:
        $\opnorm{\int f \dd E}\leq \opnorm{f}$ for any bounded Borel function $f$, 
        so $\opnorm{B-\int f \dd E} = \opnorm{B - \sum_{k=1}^N a_k E_k}<\varepsilon$ for a suitable step function $f$.
        As every $A\in \mathcal{B}(\mathcal{H})$ can be written as linear combination of two selfadjoint operators:
        \begin{equation*}
            A = \frac12\left(A + A^*\right) - \frac{i}{2}\left(iA - iA^*\right),\tag{*}\label{eq:\thegeneralthm*}
        \end{equation*}
        it suffices to check commutativity of $A$ with projection, not general elements of $\mathscr{M}^\prime$
        (because $\mathscr{M}$ is closed in the norm topology).
        \item[\ref{th:1.{12}-c} $\implies$ \ref{th:1.{12}-a}:]\page
        Any selfadjoint operator $B\in \mathcal{B}(\mathcal{H})$ can be written as a linear combination of unitaries:
        w.l.o.g. $\opnorm{B}\leq 1$ (otherwise rescale)
        \begin{equation*}
            B = \frac12\underbrace{(B+i\sqrt{I-B^2})}_{\text{unitary}} + \frac12(B-i\sqrt{I-B^2})
        \end{equation*}
        So, by \eqref{eq:1.{12}*} any $A\in \mathcal{B}(\mathcal{H})$ can be written as a linear combination of unitaries.
        So it suffices to check commutativity of $A$ with unitaries in $\mathscr{M}^\prime$.
    \end{enumerate}
\end{proof}

In other words:
A von Neumann algebra coincides with the norm closed linear span of its projections.
And any element in a von Neumann algebra can be written as a linear combination of (at most) 4 unitary operators.

\begin{corollary}\label{th:\thegeneralthm}
    $\mathscr{M}\subset \mathcal{B}(\mathcal{H})$ v. N. alg., $A\in \mathscr{M}$. If $A = U\abs{A}$ is the polar decomposition of $A$.
    (i.e. $\abs{A} = \sqrt{A^*A}$ and $U$ partial isometry) then $\abs{A}$ and $U$ are in $\mathscr{M}$.
\end{corollary}

\page

\begin{proof} $V\in \mathscr{M}^\prime$ unitary.\\
    \begin{enumerate}
        \item[] Then for $A\in \mathscr{M}, A = VAV^* = VU\underbrace{V^*V}_{=I}\abs{A}V^* = U\abs{A}$
        \item[$\implies$] (uniqueness of the polar decomposition) $\underbrace{VUV^*}_{\text{partial isometry}} = U$ and $\underbrace{V\abs{A}V^*}_{\text{positive}} = A$
        \item[$\implies$] by \ref{th:1.{12}}, $U$ and $\abs{A}$ are in $\mathscr{M}$.
    \end{enumerate}
\end{proof}

\begin{corollary}
    Every \emph{2-sided ideal} $m$ in a von Neumann algebra $\mathscr{M}$ (i.e. $AB,BA\in m$\newline$\forall A\in \mathscr{M}$, $B\in m$)
    is selfadjoint. 
\end{corollary}

\begin{proof}
    Let $B\in m$ and let $B = U\abs{B}$ be the polar decomposition. By \ref{th:1.{13}}, $U\in M$, so $U^*\in M$.
    Hence, $\abs{B}= U^*B\in m \implies B^* = \abs{B}U^*$, using the left sidedness of the ideal in the first equation and the right sidedness in the second. 
\end{proof}

\newpage

\begin{center}
    \textbf{Generating C*- and von Neumann algebras:}
\end{center}


\begin{notation*}
    The smallest von Neumann algebra containing a subset $\mathscr{X}\subset \mathcal{H}$ is denoted $\mathscr{R}(\mathscr{X})$
    and called the v. N. alg. \underline{generated} by $\mathscr{X}$.
    In general, we have $\mathscr{R}(\mathscr{X}) = \bigcap_{\mathscr{M}\text{ v. N. alg.}}\set{\mathscr{X} \in \mathscr{M}}$.
\end{notation*}

\page

\begin{definition}\index{totlaizing set}\index{separating set}\index{cyclic vector}\index{seperating vector}
    Let $\mathscr{M}$ be a von Neumann algebra $\mathscr{M}\subset \mathcal{B}(\mathcal{H}), \mathscr{V}\subset \mathcal{H}$ a subset.
    $\mathscr{V}$ is called:
    \begin{itemize}
        \item \underline{totalizing} for $\mathscr{M}$ iff. $[\mathscr{M}\mathscr{V}] = \mathcal{H}$ and
        \item \underline{separating} for $\mathscr{M}$ iff. $A\in \mathscr{M}, (Av=0\forall v\in \mathscr{V}) \implies A = 0$. 
    \end{itemize}
    A vector $v\in \mathcal{H}$ is called \underline{cyclic} (or totalizing)for $\mathscr{M}$ iff. $\set{v}$ is totalizing for $\mathscr{M}$
    and \underline{separating} iff. $\set{v}$ is separating for $\mathscr{M}$.
\end{definition}

\begin{remark*}\
    \begin{enumerate}[label=\roman*)]
        \item $\mathscr{V}$ is separating for $\mathscr{M} \iff \mathscr{V}$ is totalizing for $\mathscr{M}^\prime$ and\newline
        $\mathscr{V}$ is totalizing for $\mathscr{M} \iff \mathscr{V}$ is separating for $\mathscr{M}^\prime$.
        \item $v\in \mathcal{H}$ is cyclic $\iff P_v^\prime \coloneqq P_{[\mathscr{M}v]} \in \mathscr{M}^\prime$ satisfies $P_v^\prime = I$ and\newline
        $v\in \mathcal{H}$ is separating $\iff P_v \coloneqq P_{[\mathscr{M}^\prime v]} \in \mathscr{M}^\prime$ satisfies $P_v = I$
    \end{enumerate}
    {\color{red} Again, a beautiful proof would be very much appreciated after the corresponding homework deadline}
\end{remark*}

\page

\begin{defremark}\index{centre}\index{factor}\index{central projection}
    $\mathscr{M}\subset \mathcal{B}(\mathcal{H})$ v. N. algebra, $z \coloneqq \mathscr{M}\cap \mathscr{M}^\prime$ is called the \underline{centre}
    of the algebra $\mathscr{M}$.
    \begin{itemize}
        \item By the \hyperref[th:v.N.]{von Neumann denseness theorem}, $z$ is also the centre of $\mathscr{M}^\prime$
        (and by the same argument of all $\mathscr{M}^{(n)}$)
        \item $z$ is a (commutative) v. N. algebra (since all relevant properties are preserved under intersections).
        \item $z^\prime = \mathscr{R}(\mathscr{M}\cup \mathscr{M}^\prime)\subset \mathcal{B}(\mathcal{H})$
        (since if $A$ is a linear combination of elements of $\mathscr{M}^\prime$ and $\mathscr{M}^\dprime(=\mathscr{M})$,
        $\forall B\in z, AB = BA$, therefore $\mathscr{R}(\mathscr{M}\cup \mathscr{M}^\prime)\subseteq z^\prime$)
    \end{itemize}
A von Neumann alg. is called a \underline{factor} iff $z = \set{\lambda I}{\lambda\in\C}$\newline
A projection is called \underline{central} iff $p\in z$
\begin{itemize}
    \item In a factor the only central projections are $0$ and $1$.
\end{itemize}
\end{defremark}